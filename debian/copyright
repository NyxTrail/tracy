Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tracy
Upstream-Contact: Bartosz Taudul <wolf@nereid.pl>
Source: https://github.com/wolfpld/tracy
Files-Excluded: zstd

Files:     *
Copyright: 2017-2024 Bartosz Taudul <wolf@nereid.pl>
License:   BSD-3-Clause

Files:     import-chrome/src/json.hpp
Copyright: 2008-2009 Björn Hoehrmann <bjoern@hoehrmann.de>
           2009 Florian Loitsch <https://florian.loitsch.com/>
           2013-2022 Niels Lohmann <https://nlohmann.me>
           2016-2021 Evan Nemerson <evan@nemerson.com>
           2017-2018 The Abseil Authors
License:   Expat and Apache-2
Comment:   This file is MIT licensed, with some embedded code that is licensed
 under Apache-2.

Files:     public/libbacktrace/*
Copyright: 2012-2021 Free Software Foundation, Inc.
License:   BSD-3-clause

Files:     examples/ToyPathTracer/*
Copyright: __NO_COPYRIGHT__
License:   Unlicense

Files:     examples/ToyPathTracer/Source/enkiTS/*
Copyright: 2013 Doug Binks
License:   Zlib

Files:     dtl/*
Copyright: 2015 Tatsuhiko Kubo <cubicdaiya@gmail.com>
License:   BSD-3-Clause

Files:     imgui/*
           profiler/src/ini.c
           profiler/src/ini.h
           public/client/tracy_SPSCQueue.h
Copyright: 2004-2005 Tristan Grimmer
           2014-2023 Omar Cornut
           2016 rxi
           2020 Erik Rigtorp <erik@rigtorp.se>
           2008-2013 Kristian Høgsberg
           2010-2013 Intel Corporation
           2013 Jasper St. Pierre
           2013 Rafael Antognolli
           2015-2017 Red Hat Inc.
           2015-2017 Samsung Electronics Co., Ltd
           2018 Simon Ser
           2020 Aleix Pol Gonzalez <aleixpol@kde.org>
           2020 Carlos Garnacho <carlosg@gnome.org>
License:   Expat

Files:     imgui/imconfig.h
           imgui/misc/freetype/imgui_freetype.cpp
Copyright:
 @vuhdo (Aleksei Skriabin)
 @mikesart, improvements
 2019 @ocornut
License: CeCILL-C
 This software is governed by the CeCILL-C license under French law
 and abiding by the rules of distribution of free software. You can
 use, modify and/or redistribute the software under the terms of the
 CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 URL: "http://www.cecill.info".
 .
 As a counterpart to the access to the source code and rights to copy,
 modify and redistribute granted by the license, users are provided
 only with a limited warranty and the software's author, the holder of
 the economic rights, and the successive licensors have only limited
 liability.
 .
 In this respect, the user's attention is drawn to the risks associated
 with loading, using, modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean that it is complicated to manipulate, and that also
 therefore means that it is reserved for developers and experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards
 their requirements in conditions enabling the security of their
 systems and/or data to be ensured and, more generally, to use and
 operate it in the same conditions as regards security.
 .
 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL-C license and that you accept its terms.

Files:     imgui/imstb_truetype.h
           imgui/imstb_rectpack.h
           imgui/imstb_textedit.h
           profiler/src/stb_image_resize.h
           profiler/src/stb_image.h
           test/stb_image.h
Copyright: 2017 Sean Barrett
License:   Expat or Unlicense

Files:     nfd/nfd.h
           nfd/nfd_win.cpp
           nfd/nfd_gtk.cpp
           nfd/nfd_portal.cpp
           nfd/nfd_cocoa.m
Copyright: __NO_COPYRIGHT__
License:   Zlib

Files:     getopt/getopt.c
           getopt/getopt.h
Copyright: 2012-2023 Kim Grasman <kim.grasman@gmail.com>
License:   BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of Kim Grasman nor the
 names of contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL KIM GRASMAN BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files:     server/tracy_pdqsort.h
Copyright: 2015 Orson Peters
License:   Zlib

Files:     server/tracy_robin_hood.h
Copyright: 2018-2021 Martin Ankerl <http://martin.ankerl.com>
License:   Expat

Files:     profiler/src/imgui/imgui_impl_opengl3_loader.h
Copyright: 2013-2020 The Khronos Group Inc.
License:   Unlicense

Files:     public/client/tracy_concurrentqueue.h
Copyright: 2013-2016 Cameron Desrochers
License:   BSD-2-Clause

Files:     public/common/tracy_lz4hc.cpp
           public/common/tracy_lz4.cpp
           public/common/tracy_lz4hc.hpp
           public/common/tracy_lz4.hpp
           server/tracy_xxhash.h
Copyright: 2011-2020 Yann Collet
License:   BSD-2-Clause

Files:     public/client/tracy_rpmalloc.cpp
           public/client/tracy_rpmalloc.hpp
Copyright: 2016-2020 Mattias Jansson
License:   public-domain
 This library is put in the public domain; you can redistribute it and/or modify
 it without any restrictions.

Files:     cmake/ECMFindModuleHelpers.cmake
           cmake/FindWaylandScanner.cmake
Copyright: 2012-2014 Pier Luigi Fiorini <pierluigi.fiorini@gmail.com>
           2014 Alex Merry <alex.merry@kde.org>
License:   BSD-3-Clause

Files:     cmake/CPM.cmake
Copyright: 2019-2023 Lars Melchior and contributors
License:   Expat

Files:     debian/*
Copyright: 2024 Alan M Varghese <alan@digistorm.in>
License:   BSD-3-Clause

License:   Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
       http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License Version 2.0
 can be found in `/usr/share/common-licenses/Apache-2.0'.

License:   Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License:   BSD-2-Clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 - Redistributions of source code must retain the above copyright notice, this list of
 conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials
 provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License:   BSD-3-Clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 * Neither the name of the authors nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License:   Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgement in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or distribute
 this software, either in source code form or as a compiled binary, for 
 any purpose, commercial or non-commercial, and by any means.
 .
 In jurisdictions that recognize copyright laws, the author or authors of 
 this software dedicate any and all copyright interest in the software to
 the public domain. We make this dedication for the benefit of the public
 at large and to the detriment of our heirs and successors. We intend this
 dedication to be an overt act of relinquishment in perpetuity of all present
 and future rights to this software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to http://unlicense.org
