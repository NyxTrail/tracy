:title: tracy-import-fuchsia

NAME
====
tracy-import-fuchsia - Import Fuchsia's traces into Tracy

SYNOPSIS
========
**tracy-import-fuchsia** input.fxt output.tracy

DESCRIPTION
===========
Tracy can import traces compressed with the Zstandard algorithm (for example, using the zstd
command-line utility). Traces ending with .zst extension are assumed to be compressed.

Chrome tracing format doesn't document a way to provide source location data. The import-chrome
and import-fuchsia utilities will however recognize a custom loc tag in the root of zone begin
events. You should be formatting this data in the usual filename:line style, for example: hello.c:42.
Providing the line number (including a colon) is optional but highly recommended.

See https://fuchsia.dev/fuchsia-src/reference/tracing/trace-format for more information about
fuchsia trace format.

LIMITATIONS
===========
- Tracy is a single-process profiler. Should the imported trace contain PID entries, each PID+TID
  pair will create a new pseudo-TID number, which the profiler will then decode into a PID+TID
  pair in thread labels. If you want to preserve the original TID numbers, your traces should omit
  PID entries.

- The imported data may be severely limited, either by not mapping directly to the data structures
  used by Tracy or by following undocumented practices.

AUTHORS
=======
Maintained by Bartosz Taudul <wolf@nereid.pl>. For more information about upstream project,
see `<https://github.com/wolfpld/tracy/>`.

This man page is written for the Debian GNU/Linux system by Alan M Varghese <alan@digistorm.in>.
