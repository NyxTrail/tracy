:title: tracy-capture

NAME
====
tracy-capture - A capture application to save traces produced by a tracy client to a file

SYNOPSIS
========
**tracy-capture** -o file [-a address] [-p port] [-f] [-s seconds]

OPTIONS
=======
**-o** file
    The name of the file to write the trace to (required).

**-a** address
    Specifies the IP address of the client application (uses *localhost* if not provided).
    If no client is running at the given address, the server will wait until it can make a
    connection.

**-p** port
    Network port which should be used (optional).

**-f**
    Force overwrite, if output file already exists.

**-s** seconds
    Number of seconds to capture before automatically disconnecting (optional).

DESCRIPTION
===========
During the capture, the utility will display the following information::

        % ./capture -a 127.0.0.1 -o trace
        Connecting to 127.0.0.1:8086...
        Queue delay: 5 ns
        Timer resolution: 3 ns
        1.33 Mbps / 40.4% = 3.29 Mbps | Net: 64.42 MB | Mem: 283.03 MB | Time: 10.6 s

The *Queue delay* and *Timer resolution* parameters are calibration results of timers used by
the client. The following line is a status bar, which displays: network connection speed,
connection compression ratio, and the resulting uncompressed data rate; the total amount of
data transferred over the network; memory usage of the capture utility; time extent of the
captured data.

You can disconnect from the client and save the captured trace by pressing **Ctrl** + **C**.

AUTHORS
=======
Maintained by Bartosz Taudul <wolf@nereid.pl>. For more information about upstream project,
see `<https://github.com/wolfpld/tracy/>`.

This man page is written for the Debian GNU/Linux system by Alan M Varghese <alan@digistorm.in>.
