:title: tracy-csvexport

NAME
====
tracy-csvexport - A CLI utility to export primary zone statistice from a saved trace into
a CSV format.

SYNOPSIS
========
**tracy-csvexport** [OPTION(S)...] <trace file>

OPTIONS
=======
**-h, --help**
    Display usage information.

**-f, --filter** <name>
    Filter the zone names.

**-c, --case**
    Makes the name filtering case sensitive.

**-s, --sep** <separator>
    Customize the CSV separator (default is ",").

**-e, --self**
    Use self time (equivalent to the "Self time" toggle in the profiler GUI).

**-u, --unwrap**
    Report each zone individually; this will discard the statistics columns and instead report
    the timestamp and duration for each zone entry.

DESCRIPTION
===========
By default, the utility will list all zones with the following columns:
    + **name** - Zone name
    + **src_file** - Source file where the zone was set
    + **src_line** - Line in the source file where the zone was set
    + **total_ns** - Total zone time in nanoseconds
    + **total_perc** - Total zone time as a percentage of the programs' execution time
    + **counts** - Zone count
    + **mean_ns** - Mean zone time (equivalent to MPTC in the profiler GUI) in nanoseconds
    + **min_ns** - Minimum zone time in nanoseconds
    + **max_ns** - Maximum zone time in nanoseconds
    + **std_ns** - Standard deviation of the zone time in nanoseconds

AUTHORS
=======
Maintained by Bartosz Taudul <wolf@nereid.pl>. For more information about upstream project,
see `<https://github.com/wolfpld/tracy/>`.

This man page is written for the Debian GNU/Linux system by Alan M Varghese <alan@digistorm.in>.
