:title: tracy-profiler

NAME
====
tracy-profiler - The profiler application to view traces produced by a tracy client

SYNOPSIS
========
**tracy-profiler** file
**tracy-profiler** -a address [-p port]

EXAMPLES
========
Open a trace file named *file.tracy*::

    tracy-profiler file.tracy

Collect tracy directly from a client application running on localhost and port 8086:

    tracy-profiler -a 127.0.0.1 -p 8086

NOTES
=====
Please see section **4.2 Interactive Profiling** of the Tracy Profiler manual for detailed
documentation about the profiler application.

AUTHORS
=======
Maintained by Bartosz Taudul <wolf@nereid.pl>. For more information about upstream project,
see `<https://github.com/wolfpld/tracy/>`.

This man page is written for the Debian GNU/Linux system by Alan M Varghese <alan@digistorm.in>.
